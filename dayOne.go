package main

import "fmt"

func main(){
 	var nilai = [...]float32{
		23, 45, 67, 54, 66, 19, 56, 78, 89, 44, 11, 22, 33, 44, 55, 66, 77, 88, 99, 23, 34, 32, 23, 12,
	 }

	 fmt.Println(nilai)

	 //kumpulan ke 1
	 fmt.Println("Ini adalah kumpulan 1:")
	 var slice1 = nilai[0:8]
	 var total1 float32
	 fmt.Println(slice1)
	 min := slice1[0]
	 max := slice1[0]
	 for i, v := range slice1 {
		fmt.Println("Kumpulan ke 1, index ", i, " value ", v)
		total1 += v
		if v < min {
			min = v
		}
		if v > max {
			max = v
		}
	}

	var average1 float32 
	average1 = total1/8
	fmt.Printf("rata-rata dari kumpulan 1 adalah %.2f\n", average1)
	fmt.Println("total dari kumpulan 1 adalah", total1)
	fmt.Println("nilai max", max)
	fmt.Println("nilai min", min)

	//kumpulan ke 2
	fmt.Println("\nIni adalah kumpulan 2:")
	 var slice2 = nilai[8:16]
	 var total2 float32
	 fmt.Println(slice2)
	 min = slice2[0]
	 max = slice2[0]
	 for i, v := range slice2 {
		fmt.Println("Kumpulan ke 2, index ", i, " value ", v)
		total2 += v
		if v < min {
			min = v
		}
		if v > max {
			max = v
		}
	}
	var average2 float32 
	average2 = total2/8
	fmt.Printf("rata-rata dari kumpulan 2 adalah %.2f\n", average2)
	fmt.Println("total dari kumpulan 2 adalah", total2)
	fmt.Println("nilai max", max)
	fmt.Println("nilai min", min)
	 
	//kumpulan ke 3
	fmt.Println("\nIni adalah kumpulan 3:")
	 var slice3 = nilai[16:24]
	 var total3 float32
	 fmt.Println(slice3)
	 min = slice3[0]
	 max = slice3[0]
	 for i, v := range slice3 {
		fmt.Println("Kumpulan ke 3, index ", i, " value ", v)
		total3 += v
		if v < min {
			min = v
		}
		if v > max {
			max = v
		}
	}
	var average3 float32 
	average3 = total3/8
	fmt.Printf("rata-rata dari kumpulan 3 adalah %.2f\n", average3)
	fmt.Println("total dari kumpulan 3 adalah", total3)
	fmt.Println("nilai max", max)
	fmt.Println("nilai min", min)

	//nilai tertinggi
	if total3 < total1 && total1 > total2 {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 1", slice1, "dengan total", total1)
	} else if total3 < total2 && total2 > total3 {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 2", slice2, "dengan total", total2)
	} else {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 3", slice3, "dengan total", total3)
	}

	//nilai terendah
	if total3 > total1 && total1 < total2 {
		fmt.Println("kumpulan dengan total terendah adalah slice 1", slice1, "dengan total", total1)
	} else if total3 > total2 && total2 < total3 {
		fmt.Println("kumpulan dengan total terendah adalah slice 2", slice2, "dengan total", total2)
	} else {
		fmt.Println("kumpulan dengan total terendah adalah slice 3", slice3, "dengan total", total3)
	}
}